# KDisplay

KDisplay is a display management software for KDE Plasma Workspaces.

## End user
Please contact the support channels of your Linux distribution for user support first. In case
the bug is traced back to Disman, you can create an [issue ticket][issue].

## Contributing
See the `CONTRIBUTING.md` file.

[issue]: https://gitlab.com/kwinft/kdisplay/-/issues
