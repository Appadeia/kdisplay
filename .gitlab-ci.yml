stages:
  - Compliance
  - Build
  - Test

variables:
  IMAGE_BASE: ${CI_REGISTRY}/kwinft/ci-images/archlinux/kwinft-base

workflow:
  rules:
    - when: always


Message lint:
  stage: Compliance
  image: node:latest
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: always
    - if: '$CI_COMMIT_BRANCH == "master" || $CI_COMMIT_BRANCH =~ /^Plasma\// || $CI_COMMIT_TAG'
      when: never
    - when: always
  variables:
    UPSTREAM: https://${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD}@${CI_SERVER_HOST}/kwinft/kdisplay.git
  script:
    - if [ -n "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" ];
      then export COMPARE_BRANCH=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME; else export COMPARE_BRANCH=master; fi
    - "echo Branch to compare: $COMPARE_BRANCH"
    - yarn global add @commitlint/cli
    - yarn add conventional-changelog-conventionalcommits
    - git remote add _upstream $UPSTREAM || git remote set-url _upstream $UPSTREAM
    - git fetch -q _upstream $COMPARE_BRANCH
    - commitlint --verbose --config=ci/compliance/commitlint.config.js --from=_upstream/$COMPARE_BRANCH
  cache:
    paths:
      - node_modules/

clang-format:
  stage: Compliance
  image: ${IMAGE_BASE}-master:latest
  script:
    # Run clang-format via wrapper script.
    - bash ci/compliance/clang-format.sh


.common-master: &common-master
  image: ${IMAGE_BASE}-master:latest
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^Plasma\// || $CI_COMMIT_TAG'
      when: never
    - when: on_success

.common-stable: &common-stable
  image: ${IMAGE_BASE}-stable:latest
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^Plasma\// || $CI_COMMIT_TAG'
      when: on_success
    - when: never


.common-build: &common-build
  stage: Build
  artifacts:
    paths:
      - disman-bench
      - ci-build
    expire_in: 1 week

Master build:
  <<: *common-build
  <<: *common-master
  script:
    # For now we build Disman beforehand.
    - mkdir -p disman-bench/build && cd disman-bench
    - git clone --branch master https://gitlab.com/kwinft/disman.git src
    - cd build
    - cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr ../src
    - make -j$(nproc)
    - make install -j$(nproc)
    - cd ../..
    # Build of KDisplay
    - mkdir ci-build && cd ci-build
    - cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr ../
    - make -j$(nproc)
    - make install -j$(nproc)

Stable build:
  <<: *common-build
  <<: *common-stable
  script:
    # For now we build Disman beforehand.
    - mkdir -p disman-bench/build && cd disman-bench
    - PLASMA_VERSION=${CI_COMMIT_BRANCH}
    - if [ -z "${PLASMA_VERSION}" ];
      then PLASMA_VERSION=$(echo "${CI_COMMIT_TAG}" | sed -e 's,kdisplay@\([^\.]*\.[^\.]*\)\..*,Plasma/\1,g'); fi
    - echo $PLASMA_VERSION
    - git clone --branch ${PLASMA_VERSION} https://gitlab.com/kwinft/disman.git src
    - cd build
    - cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr ../src
    - make -j$(nproc)
    - make install -j$(nproc)
    - cd ../..
    # Build of KDisplay
    - mkdir ci-build && cd ci-build
    - cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr ../
    - make -j$(nproc)
    - make install -j$(nproc)


.common-test: &common-test
  stage: Test
  script:
    # Install Disman.
    - cd disman-bench/build
    - make install -j$(nproc)
    - cd ../..
    # KDisplay autotesting starts here.
    - cd ci-build
    - Xvfb :1 -ac -screen 0 1920x1080x24 > /dev/null 2>&1 &
    - export DISPLAY=:1
    - export WAYLAND_DEBUG=1 MESA_DEBUG=1 LIBGL_DEBUG=verbose
    - export QT_LOGGING_RULES="*=true"
    - ctest -N
    - dbus-run-session ctest --output-on-failure

Master autotests:
  needs:
    - job: Master build
      artifacts: true
  <<: *common-test
  <<: *common-master

Stable autotests:
  needs:
    - job: Stable build
      artifacts: true
  <<: *common-test
  <<: *common-stable
